package de.tadris.fitness.util.weather

import de.tadris.fitness.util.WorkoutLogger
import java.net.URL
import org.json.JSONException
import org.json.JSONObject

class OWMWeather {
    companion object {
        var OWM_API_KEY = ""
        private const val OWM_API_ENDPOINT =
            "http://api.openweathermap.org/data/2.5/weather?units=metric&" // TODO: make configurable unit system

        @JvmStatic
        fun retrieve(lat: Double, lon: Double): WeatherPoint? {
            var result: WeatherPoint? = null
            if (OWM_API_KEY.isEmpty()) {
                WorkoutLogger.log("OWMWeather", "No API key set")
                return null
            }

            val url = "${OWM_API_ENDPOINT}lat=$lat&lon=$lon&appid=$OWM_API_KEY"
            val json = URL(url).readText()

            // parse json result
            JSONObject(json).let {
                try {
                    val main = it.getJSONObject("main")
                    val weather = it.getJSONArray("weather").getJSONObject(0)
                    val wind = it.getJSONObject("wind")
                    val clouds = it.getJSONObject("clouds")

                    result = WeatherPoint(
                        main.getDouble("temp"),
                        main.getInt("pressure"),
                        main.getInt("humidity"),
                        wind.getDouble("speed"),
                        wind.getInt("deg"),
                        clouds.getInt("all"),
                        weather.getString("main"),
                    )
                } catch (e: JSONException) {
                    WorkoutLogger.log("OWMWeather", "error parsing weather data: ${e.message}")
                }

                return result;
            }
        }

        @JvmStatic
        fun isValidOWMAPIKey(key: String): Boolean {
            return key.isNotEmpty() && key.length == 32 && key.regionMatches(0, "0123456789abcdef", 0, 32)
        }
    }
}