package de.tadris.fitness.util.weather

class WeatherPoint {
    val temperature: Double?
    val pressure: Int?
    val humidity: Int?
    val windSpeed: Double?
    val windDirectionDegrees: Int?
    val cloudiness: Int?
    val status: String?

    constructor(temperature: Double, pressure: Int, humidity: Int, windSpeed: Double, windDirectionDegrees: Int, cloudiness: Int, status: String) {
        this.temperature = temperature
        this.pressure = pressure
        this.humidity = humidity
        this.windSpeed = windSpeed
        this.windDirectionDegrees = windDirectionDegrees
        this.cloudiness = cloudiness
        this.status = status
    }
}